## Informations sur la reproduction du bug

_Remplace le texte en italique (entre \_ \_) pour chaque information._

- **Environnement(s) :** _Production, développement, local... Précise tous les environnements sur lequel le bug a été reproduit._
- **Navigateur(s) utilisé(s) :** _Chrome, Firefox, Safari, Edge, ... Précise tous les navigateurs sur lequel tu as rencontré le bug. Si le bug n'arrive pas sur certains navigateurs, cette information peut aussi être précisée ici._
- **Rôle(s) :** _Responsable de structure, Référent, Agent d'accueil, ... Précise tous les rôles avec lesquels tu as rencontré le bug._
- **Type(s) de structure :** _CCAS, Ministère, Association, ... Précise toutes les typologies de structure avec lesquelles tu as rencontré le bug._
- **Lien vers la page :** _Tu peux rajouter le lien vers la page sur laquelle a été rencontré le bug, ou l'identifiant de la fiche bénéficiaire, de la synthèse d'entretien ou de l'instruction de dossier, ... **⚠️ Si le bug est sur l'environnement de production et que tu mentionnes une fiche bénéficiaire, pense à marquer le ticket comme confidentiel !**_

## Description du bug

_Décris le bug rencontré ici..._

/label ~"Label::🐛 bug"
/assign @Clara.DUmont
