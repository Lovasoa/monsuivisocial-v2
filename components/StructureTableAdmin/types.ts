import type { AdminStructuresOutput } from '~/server/trpc/routers'

export type Structures = AdminStructuresOutput['structures']
