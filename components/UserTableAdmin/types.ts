import type { AdminUsersOutput } from '~/server/trpc/routers'

export type Users = AdminUsersOutput['users']
