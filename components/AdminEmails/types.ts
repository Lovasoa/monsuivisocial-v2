import type { AdminEmailsOutput } from '~/server/trpc/routers'

export type Emails = AdminEmailsOutput['emails']
