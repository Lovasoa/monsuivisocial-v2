/*
  Warnings:

  - You are about to drop the column `post_office_box` on the `beneficiary` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "beneficiary" DROP COLUMN "post_office_box";
