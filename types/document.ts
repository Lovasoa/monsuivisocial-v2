export enum DocumentTag {
  Emploi = 'Emploi',
  SanteHandicap = 'SanteHandicap',
  MaintienADomicile = 'MaintienADomicile',
  BudgetRessources = 'BudgetRessources',
  Logement = 'Logement',
  Impot = 'Impot',
  Justice = 'Justice',
  Retraite = 'Retraite',
  PrestationsSociales = 'PrestationsSociales',
  Identite = 'Identite'
}
