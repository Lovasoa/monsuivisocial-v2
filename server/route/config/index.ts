export * from './getAgents.query'
export * from './getBeneficiariesCities.query'
export * from './getExaminationDates.query'
export * from './getInstructorOrganization.query'
export * from './getPrescribingOrganizations.query'
export * from './getStructureFollowupTypes.query'
