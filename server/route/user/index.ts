export * from './acceptCGU.mutation'
export * from './addUser.mutation'
export * from './getUsers.query'
export * from './updateUser.mutation'
