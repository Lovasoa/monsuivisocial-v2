export * from './add-comment.mutation'
export * from './edit-comment.mutation'
export * from './delete-comment.mutation'
