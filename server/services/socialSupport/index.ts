import {
  getEditionPermissions,
  getCreationPermissions
} from './permission.service'

export const SocialSupportService = {
  getEditionPermissions,
  getCreationPermissions
}
