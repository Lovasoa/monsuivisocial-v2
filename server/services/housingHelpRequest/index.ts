import { create } from './create.service'
import { get } from './get.service'
import { getEditionPermissions } from './permission.service'
import { update } from './update.service'

export const HousingHelpRequestService = {
  create,
  get,
  getEditionPermissions,
  update
}
