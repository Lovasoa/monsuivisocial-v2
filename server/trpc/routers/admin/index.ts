export type {
  AdminStructuresOutput,
  AdminUserOutput,
  AdminUsersOutput,
  AdminEmailsOutput
} from '~/server/route/admin'
