export { beneficiaryFieldLabels } from './beneficiary.labels'
export { userFieldLabels } from './user.labels'
export { followupTypeFieldLabels } from './followup-type.labels'
export { followupFieldLabels } from './followup.labels'
export {
  structureFieldLabels,
  adminStructureFieldLabels
} from './structure.labels'
export { helpRequestFieldLabels } from './helpRequest.labels'
export { relativeFieldLabels } from './relative.labels'
export { documentFieldLabels } from './document.labels'
export { housingHelpRequestFieldLabels } from './housingHelpRequest.labels'
