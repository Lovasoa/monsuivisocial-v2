export function minutesToMillis(minute: number) {
  return minute * 60 * 1000
}
