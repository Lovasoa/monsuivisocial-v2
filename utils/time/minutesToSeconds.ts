export function minutesToSeconds(minute: number) {
  return minute * 60
}
