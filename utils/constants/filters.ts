export enum FilterType {
  Date = 'date',
  SelectOne = 'selectOne',
  SelectMultiple = 'selectMultiple',
  Search = 'search'
}
