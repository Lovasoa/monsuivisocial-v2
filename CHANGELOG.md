# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.14.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.14.0...v1.14.1) (2024-01-23)


### Bug Fixes

* **statistique:** Calcule des aides financières: ([213a9ad](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/213a9ad0d711a371510de70032787d7e130249fc))

## [1.14.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.12.0...v1.14.0) (2024-01-22)


### Features

* Affiche la version du client dans le pied de page ([8f9da19](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8f9da196782389eb334fe31d08d6317da697ec1d))
* Affiche la version du client dans menu latéral ([c6a8223](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c6a8223841826e26f3801aec5d7c360117393fb2))
* Affiche une erreur lorsque les caractères < et > sont utilisés dans un texte libre ([c802e30](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c802e301cfefda118b130b45e6b81428f551a676)), closes [#202](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/202)
* **beneficiary:** Ajoute le champ département ([82fffb2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/82fffb2a23bc4a1be83cca4be3a3a6120306524b)), closes [#289](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/289)
* **document:** Affiche une erreur lorsque l'ajout d'un document échoue ([05f7683](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/05f76832b29fc6764fab6fae0d0e66b85c45634e))
* Gestion de la déconnexion après une mise en vielle ([6a46386](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6a463864227efcdb95549c904594a8f75413fb22))
* Gestion du rechargement de l'application après publication d'une mise à jour ([4b03b04](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b03b044c134a51775967783b3befda5ca2d2fb4))
* **history:** Déplie l'élément visé de l'historique du bénéficiaire lorsque accédé depuis la page des Accompagnements ([f0cc30d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f0cc30dee278e0ea6c700babbdf9a370355b6936)), closes [#284](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/284)
* **instruction:** Ajoute une modale pour sélectionner le type d'instructions de dossier que l'on veut créer ([829c8d3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/829c8d3a9a2856e02268836acf6564542f0cfe21))
* **instruction:** Reprend la mise en page du formulaire d'instruction de dossier d'aide financière ([15bb1ac](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/15bb1ac645f1b34a521e099e5a4adc7779778d8b)), closes [#246](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/246) [#245](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/245)
* **logging:** Ajout de la journalisation des adresses IP ([093149b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/093149bd5f3311dcee343bc511d0f7457f7c7265))
* **matomo:** Ajoute un événement de suivi lors de l'accès à la page de création de bénéficiaire depuis le tableau de bord ([4dee2dd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4dee2ddc890f126a63af669321be23c628185079)), closes [#280](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/280)
* **ministry:** Ajoute le nouveau ministre suite au remaniement ([13771b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/13771b2ed3e6e072a9357516f74b0f26133583eb))
* Retire le droit de suppression d'un bénéficiaire aux référents ([b38912d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b38912dcad99a5a3e601e051294ab89609ed51d7))
* **securite:** Augmente à 300 le nombre limite de requêtes avant que le serveur soit saturé ([ab4eb51](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ab4eb51bf843c8003e17375aa09c07793e41ea71))
* **stats:** Ajoute le filtre par statut pour les statistiques de bénéficiaires ([32a4863](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/32a4863e076e49405abf0095dade0c92804f872e)), closes [#283](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/283)
* **stats:** Ajoute les statistiques par département et par ministre ([98b2df6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/98b2df6e8ac9ad9ab4a395c3fccfaafa40afa3e2)), closes [#282](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/282)
* **structure:** Supprime les types d'accompagnement associés à une structure lorsque celle-ci est supprimée ([45ae4ba](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/45ae4ba319e5d74fc768c5a9a416dbb3b5df662a))


### Bug Fixes

* **a11y:** Permet aux lecteurs d'écran d'annoncer les messages suite à la soumission d'un formulaire ([acb236b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/acb236b8ed607233cf560d00cef7076d2bcf2fd9))
* Agent d'accueil ne peut pas modifier une instruction de dossier ([67711ef](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/67711ef3ad2fc43c529be7dae4d79a8ce903db3f))
* **beneficiaire:** Corrige un bug du comportement déplié/replié des éléments de l'historique d'une fiche bénéficiaire lorsque l'onglet actif était changé ([bed69bf](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bed69bfa959a94ec9eef30ee03abce577989383d)), closes [#273](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/273)
* **beneficiaire:** Corrige un bug qui empêche l'affichage des flèches de navigation en bas du formulaire d'édition d'une fiche bénéficiaire ([dfc5ded](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dfc5ded6433f120f1b9e5c5d99d36424e99665f5))
* **beneficiary:** Corrige un bug qui empêche de filtrer les bénéficiaires par tranche d'âge ([10410ef](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/10410efc1e77b7690b7db61885469e8f2a6f90ca))
* Correction du calcul des statistiques des demandes d'aide financières ([3d385aa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3d385aa53adabcc80a3d89a5cb589157379946c2))
* Corrige un bug qui empêche la création d'un nouvel organisme prescripteur ([82b6e35](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/82b6e35fd62c31131878ce8a456ffdc3e236b09f))
* **document:** L'état de chargement apparaît suite à la modification ou la suppression d'un document ([a4a8817](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a4a8817b544703e67276a72a68892460aa96eef9)), closes [#305](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/305)
* **foyer-fiscal:** Corrige la modification des membres du foyer fiscal ([9b9fdfe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9b9fdfeb43f7252faa3aa86470c719af743ea177))
* **history:** Un référent peut exporter les synthèses d'entretien et les instructions de dossier ([302882f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/302882fd1dfb026c59a80daa46341cfc9e9df1e6))
* **instructions:** Impossible de modifier le montant alloué depuis le tableau des instructions ([9a9904e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9a9904eedd0c93d3cb8b219a6b450368aff80ad7))
* Corrige un bug qui empêche l'initialisation de certains filtres avec leur valeur précédemment sélectionnée ([2b923ed](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b923ede228f86eec67204073d0441da7a9e1ab2)), closes [#254](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/254)
* **notification:** Corrige un bug sur les notifications de commentaire ([2b75ae6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b75ae64ec5eaa2f27e8b7d29e08c75d9bc0c546))
* Présentation par ordre décroissant des stats de types de suivi ([f781a1a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f781a1a7e2aaa1e5ecc95ec80946295d5e4e1c2b))
* **security:** Consolide la configuration des Content Security Policy ([80128b1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/80128b18f40135d8a32e9fa4e028bb6e2f22d755))
* **social-support:** affiche le séparateur avec les commentaires dès lors qu'on peut ajouter un commentaire ([5a4a31b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5a4a31b40a1b82f505cee48dff297f5c902bb9b8))

## [1.13.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.12.0...v1.13.0) (2023-12-19)


### Features

* Affiche la version du client dans le footer ([2a10399](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2a10399de213bfd92c6f6e2636eb792c6af30b45))
* Affiche la version du client dans menu droite ([8f88640](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8f886400270480ba3cca2812853e0239814a7e16))
* **logging:** Ajout d'un middleware pour logger les adresses IP ([0ed7df1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0ed7df18110c6157908472ef01c46fe3487314b4))
* **securite:** Augmente à 300 le nombre limite de requetes avant 429 ([a55664e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a55664e508d09c1caec39aa4d4bd02cdd607afab))

## [1.12.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.11.1...v1.12.0) (2023-12-13)


### Features

* **beneficiaire:** Affiche des champs relatifs aux ressources des autres membres du foyer en fonction de l'ajout de membres au foyer fiscal de la fiche ([302745f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/302745ffd5f67f5b2c35abc2b0d13fa24e1983da)), closes [#241](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/241)
* Être notifié quand un.e collègue ajoute un suivi à un bénéficiaire dont je suis référent ([faafb10](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/faafb1045ed803c00e51d0a71de3246ca2fe882a)), closes [#239](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/239)
* Gestion de message d'alerte avant déconnection ([66a729d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/66a729d7056156c821dcdcc6ab0209b7f4c830fb))
* Gestion du rafraichissement du jeton de sécurité ([40257e4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/40257e49c5f238edc8aea07e4781185c1a5da372))
* **instruction:** Ajoute une modale pour sélectionner le type d'instructions de dossier que l'on veut créer ([52c5d90](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/52c5d901b07c511f580ac43327ee2910765ece4a)), closes [#243](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/243) [#247](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/247)
* **instruction:** Renomme instruction de demande d'aide en instruction de dossier partout dans l'application ([0e41e16](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0e41e168f6976fdae711f08a3fb7da7f65f5148b)), closes [#237](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/237)
* Marque le cookie en http-only ([1b70e0e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1b70e0e994f509144fa1de583fcf0f8890edf980))
* Mise à jour de nuxt-security ([11a138c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/11a138ccc519c10ac6436643f77635ad89327842))
* statistics contrast ([da65e55](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/da65e550ef63d8b209a1d492a29415f2dff47a5e)), closes [#251](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/251)
* **stats:** Afficher la répartition des bénéficiaires par villes ([7976819](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7976819bb806571e19d7c9f59733c4318fc953ad)), closes [#240](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/240)


### Bug Fixes

* **auth:** Renvoie correctement une erreur lorsque le retour de l'authentication d'InclusionConnect ne présente pas les paramètres attendus ([4a2d97b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4a2d97b7a6921b7241f3c8033afc17adef981d91)), closes [#262](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/262) [#265](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/265)
* **auth:** Reprise de la validatation du token pour éviter un freeze de la page ([624969e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/624969e3fd2d6a95182859143c47f2c5ac7fee9c))
* **beneficiaire:** Corrige un bug qui rendait cliquable le lien vers un document confidentiel auquel l’utilisateur n’a pas accès depuis un élément de l’historique du bénéficiaire. ([504ad5f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/504ad5fdd2bdbda55fcfdc7673e0e241d48cd36e)), closes [#272](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/272)
* Correctifs mineurs ([c21b5ca](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c21b5ca9ffb6af291b8e91a8a8ce9b4eb2f6913e))
* Correction et amélioration des vérifications de validité du jeton ([06d2a98](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/06d2a987b44b8330ff6582286c4a19b63c90c132))
* Corrige un bug qui empêche de retirer une date déjà saisie ([2b9d6bb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b9d6bb99a7f54737392518325c65002c1a0aef4)), closes [#270](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/270)
* Corrige un bug qui empêche de sélectionner une adresse dans un DROM dans les formuaires ([b6a44e8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b6a44e8c27e4311e71b2fd3eca9a8eaff0e896bf)), closes [#275](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/275)
* Désactive la modale de choix de type d'instruction de dossier ([3aee473](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3aee4731f941fd2afef1f663082de06343e4d738))
* **document:** ajout d'un documnt closes [#263](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/263) ([baba8d4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/baba8d404449a6a323225ee554f58ff8ef3b6807))
* **matomo:** enable matomo on dev environment ([0f48434](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f484340cc008a016e2c3ec916566f4d989bc294))
* Modification de la durée de validation du jeton de sécurité ([dc9a04c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dc9a04c0ff8331e63d0298cfb784bd57388af183))

### [1.11.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.11.0...v1.11.1) (2023-12-11)


### Bug Fixes

* **admin:** Impossible de créer un type d'accompagnement facultatif sur une structure ([58c68c9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/58c68c9de1e42273e4953d18c5de4834e19ca2bd)), closes [#266](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/266)
* **auth:** Renvoie correctement une erreur lorsque le retour de l'authentication d'InclusionConnect ne présente pas les paramètres attendus ([79d42b7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/79d42b723a173b973a7eb33b777e5ef98d6fe982)), closes [#262](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/262) [#265](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/265)
* **auth:** Reprise de la validatation du token pour éviter un freeze de la page ([65799f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/65799f7a26ab12091357e1355054de13834af8e9))
* **documents:** Améliore la mise en page des filtres ([94c11cb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/94c11cb05c6c53137f077adeb270bbe6ef888948)), closes [#261](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/261)

## [1.11.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.10.2...v1.11.0) (2023-12-05)


### Features

* Cache les champs signalisations et interventions des suivis ([6a06fb4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6a06fb442b708be1f03c81783b78523254a72c1b)), closes [#234](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/234)
* Mise à jour de nuxt-security ([eca6ca9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/eca6ca9b9f141a0af0c61da0d347a9163257be7c))
* Mise à jour des versions mineurs des paquets ([534edf4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/534edf40c8b047aa301eec752df0ddfaf23e4564))


### Bug Fixes

* **admin:** Affiche un état de chargement lors de l'authentification à l'espace d'administration ([b8da168](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b8da16830a7d1a7b4f5a517a6adee21f0ee30578))
* **admin:** Corrige l'envoi de l'invitation à l'atelier premiers pas aux utilisateurs créés 7 jours plus tôt ([6937b4e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6937b4e0725abf37de2dde959ec019e15c7b400f))
* **admin:** Impossible de mettre à jour les types d'accompagnement des structures ([55dbe61](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/55dbe61ab8c6ebee78ca6f556bf471935e9519cf)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* **admin:** La récupération de l'admin authentifié depuis les cookies génère une erreur ([4db97c1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4db97c108d6e834410db06840428458cc82306aa)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* **auth:** L'application ne gère pas le cas où le jeton d'authentification n'a pas pu être récupéré auprès d'InclusionConnect ([46227d9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/46227d914727c886610a48d69c8b4127e102b0d7)), closes [#230](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/230)
* **beneficiaire:** Affiche toujours 3 filtres par défaut ([ec4297e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ec4297eeaf9f4c4d905c4215e1b18bfbd67a51f4)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* **beneficiaire:** Lors de la modification du numéro de téléphone du bénéficiaire, l'information était effacée ([6276e27](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6276e2762520b13f0a87f7501f9ebc1945b0c48a)), closes [#226](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/226)
* **beneficiaire:** Quand le tri par date de naissance est appliqué, les fiches sans dates de naissance sont toujours triées en dernier ([1b29222](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1b29222b928c1ad1cb69d847e966ac52a436fd30)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* closes [#227](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/227) ([23dd319](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/23dd3190354312f60767a648f259fdd2c3e45e42))
* closes [#227](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/227) ([94556ae](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/94556ae512a32acca2d10c940ad84d83d0360f2e))
* closes [#228](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/228) ([039ec23](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/039ec238a5ffecd87d87687469e705afe5082dff))
* Déplace la génération du contenu des emails coté serveur ([d504e45](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d504e45c3d71399f411eb11c5652632a2be84cea))
* Do not throw an error during trpc context creation if the user is not found ([d62d705](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d62d705efed40aa86e96284816159c71f040aa4a))
* **export:** closes [#233](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/233) ([711d84d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/711d84d5c36da6ce626a065bbf5c2b1a03617709))
* Gestion d'un utilisateur connu de IC et pas de MSS ([8bfd380](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8bfd380e8517db633c26fd00235e8506cf6cbc4e))
* Gestion de la contrainte unique lors de la création d'un type de suivi ([62aba47](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/62aba474da20c502781f06c80b1bd4bf97170256))
* issue[#252](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/252) ([17cc65f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/17cc65fa6d39e74c43babe9a0e39dac1fbf02f12))
* **matomo:** set cross-origin-embedder-policy to unsafe ([e9af7b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e9af7b2423846ff12ea03c258b1f13b1b85a1845))
* **nuxt:** ptimizing dependencies configuration ([a2fe16a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a2fe16a75387aec7eb0675c6697486b24f0f0d93))
* Sauvegarde de Réorienté après l'entretien ([204c08a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/204c08a9c56e7ce38baee120313245cab114344e))
* **structure:** Corrige l'affichage d'erreur lorsqu'aucun type d'accompagnement n'est sélectionné lors de la modification de la structure ([0feb692](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0feb692cd7899dddf921244b231fdbe5bc74b20d)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* Supprime les espaces et mets en mininuscule l'email de l'utilisateur ([f8e7a30](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f8e7a305c439003102ef396a4fd36c98779af5fc))

### [1.10.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.10.1...v1.10.2) (2023-11-28)


### Bug Fixes

* **admin:** Corrige l'envoi de l'invitation à l'atelier premiers pas aux utilisateurs créés 7 jours plus tôt ([ecdca56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ecdca5612071cbf33a4386fde0c702307709ab52))
* **admin:** Impossible de mettre à jour les types d'accompagnement des structures ([afdaef6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/afdaef6e93907a946a6d319ad7a60aeead18b214)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* **admin:** La récupération de l'admin authentifié depuis les cookies génère une erreur ([1dff780](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1dff7806c6a4c532a7f6b3a7289b923169cae6e1)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* **structure:** Corrige l'affichage d'erreur lorsqu'aucun type d'accompagnement n'est sélectionné lors de la modification de la structure ([8605bf3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8605bf357aa4f7f3edc1c1dd34e4ca738a294c1b)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)

### [1.10.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.10.0...v1.10.1) (2023-11-23)


### Bug Fixes

* **beneficiaire:** Affiche toujours 3 filtres par défaut ([be17a99](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/be17a99c6e6e0f6b77a35100328e8a67290c1b46)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* **beneficiaire:** Lors de la modification du numéro de téléphone du bénéficiaire, l'information était effacée ([43a542e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/43a542e0fddd3fb4f4417e64617502fb88875113)), closes [#226](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/226)
* **beneficiaire:** Quand le tri par date de naissance est appliqué, les fiches sans dates de naissance sont toujours triées en dernier ([2c467fe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2c467fe0ee0ddfce6bc9f5b7b5d825bd966c6c19)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* closes [#227](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/227) ([4b1c034](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b1c034d9d059718d92d1c41faee9c6cbccf10a3))
* closes [#227](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/227) ([3c7d7bb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3c7d7bbc162f9228062441bd7d2782db947f5e14))
* closes [#228](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/228) ([af18045](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/af180459ddf2d9f2062e2a3dc68917df393bce33))
* Déplace la génération du contenu des emails coté serveur ([fec1b3b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fec1b3b5bc04733107aea56fc9d0562f09ba8a93))
* Gestion d'un utilisateur connu de IC et pas de MSS ([e23fe3a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e23fe3a773ce73bec0f052dce382efc3461261e8))
* Supprime les espaces et mets en mininuscule l'email de l'utilisateur ([b424503](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b424503ed15e684f6f209479edddd61d7ef1af02))

## [1.10.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.9.0...v1.10.0) (2023-11-21)


### Features

* **beneficiaire:** Permet aux utilisateurs avec le rôle Travailleur social ou Référent de supprimer une fiche bénéficaire à laquelle ils ont accès ([dee2b51](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dee2b513ae2184153bc8d1c4bea9368e152398a1)), closes [#211](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/211)
* **beneficiaires:** Reprend le design de la page qui liste les bénéficiaires de la structure ([ee856c5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ee856c512b0826cb00200465fab29ccfe326f7b1)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* **statistiques:** Ajoute un filtre par référents dans les statistiques ([4d7c5d1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4d7c5d18e4c42c8843cef1aed282cc8905ba8290)), closes [#213](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/213)
* **utilisateur:** Envoie aux utilisateurs créés il y a une semaine un mail d'invitation à l'atelier "Premiers pas" ([45a02bf](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/45a02bf548494a8db1a08551c45d29f4f9948421)), closes [#212](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/212)


### Bug Fixes

* **beneficiarie:** L'identité du bénéficiaire n'est pas affichée dans le fil d'Ariane ([e3628b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e3628b26e943d027862675d02e9c695755fbf0c8)), closes [#214](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/214)
* Création d'un bénéficiaire avec un membre du foyer lié ([0ea892d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0ea892d51adaf52bcf110d455f5ef98d7eeb9c7b))

## [1.9.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.7.0...v1.9.0) (2023-11-15)


### Features

* Ajout d'un bandeau dans le formulaire de santé ([47038b6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/47038b6d73767add43a10adf9a97346c44cde66c))
* Ajout d'une API health check ([99b4b45](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/99b4b452d46af1fe92483bab49c9278063e59813))
* Ajout des constraintes d'accès aux données ([3ed6c02](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3ed6c0222f1f711264184415cf39855b8a4a6472))
* Ajout des permissions pour les documents ([1f95281](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1f95281a6f22c017bef4758236de3b0cd468f996))
* Ajout du type de structure EFS ([f7ded12](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f7ded120a43f73bdcb8141cbea3c7fc049fcf9aa))
* **beneficiaire:** Affiche une alerte lorsque l'utilisateur quitte le formulaire de la fiche bénéficiaire afin de préciser que les informations ne seront pas sauvegardées ([c0d0a97](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c0d0a974b0e060190c0367010c4d5ad327ed7b0c)), closes [#148](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/148)
* **beneficiaire:** Mets à jour le design du bloc invitant à compléter les informations du bénéficiaire sur le formulaire simplifié ([6fe8564](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6fe8564bc547493dc25d20f94997a2a8cb554c32)), closes [#191](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/191)
* design corrections for beneficiary page ([47ef8a0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/47ef8a0b5e85e3bd12b89f2ef63ff8a7d29b7996)), closes [#194](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/194)
* **formulaire:** affiche un bloc d'erreur qui résume toutes les erreurs présentent dans le formulaire ([2b8d1e9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b8d1e90d91542f2f24c135ce979c9301ec1f5a1)), closes [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/67)
* Gestion des règles de la page bénéficiaire via les permissions ([28c0769](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/28c0769d69f26c1e525ab70a7bbbf45e7429d4c9))
* **help-request:** closed by agent status ([44dca2a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/44dca2a7dcf670fec1a3a99ecaf3b5bb8dc18086)), closes [#171](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/171)
* Historique du changement des données ([1a4c6e6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1a4c6e6c383d01c6a7e4df582ec29bf3719f249b)), closes [#57](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/57)
* Introduction des contraintes sur les dépots des données ([d3d6d1b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d3d6d1b83e29bd9b118e384f908b0d2daab5a24e))
* Mise à jour de la règle de visibilité des informations de santé ([94a621c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/94a621c2db16736e972569ccfb5f061954741107)), closes [#163](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/163)
* **prisma:** Usage des écritures imbriquées au lieu des transactions ([3b98f2c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3b98f2cda225159270b66a409de974f09a398825))
* **security:** Utilisation des permissions pour la création d'un bénéficiaire ([72295d6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/72295d6cd4e4c1daca20c98428b77c2c4ede019f))
* **security:** Vérification de la permission de changer un champ ([4e7dc61](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4e7dc61d9ad9e851037f9213948680e953955b08))
* **structure:** ajoute un mail de bienvenue à la création d'un espace pour une nouvelle structure ([d9d7209](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d9d720964548e189db52ee9d8f02c78bf1b4d1f6)), closes [#203](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/203)
* Suppression des guards et ajout des permissions par type de structure ([f885723](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f885723764713fa62ee3e8f5a1ccc062bd8ca75b))
* Une commune n'a pas accès au NIR ([a4445ad](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a4445ad0dc977b87d1ce521dbf3c21af89fe762e))
* Utilisation d'une vue sql pour les types de suivi ([bb6ebf8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bb6ebf8f296e2860172426ec46dcb3ba2fa49208))
* Utilisation des permissions pour la création du bénéficiaire et gestion des types de structures ([6d79df5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6d79df5b46bc94dca72e67ecaec021dcf6632271))


### Bug Fixes

* Affichage d'un bandeau informant du problème d'envoi d'email ([faa7ad4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/faa7ad417ff981b54050620d071a1f99b50b59dd))
* Affichage des commentaires dans l'historique d'un bénéficiaire ([4990d47](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4990d47a3d3c7192e5a6a2064312188a2aef68ca))
* Améliore les erreurs affichées sur plusieurs formulaires ([a93d774](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a93d77479d4f4c530efee0eb5b3e7da4e9b91eeb)), closes [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/67)
* **beneficiaire:** Affiche l'alerte de fermeture lorsque l'utilisateur quitte le formulaire via le bouton Annuler ([15c5afe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/15c5afe7598c4ffffb4267c5f42aec5626d182a0))
* **beneficiaire:** l'impression de la fiche bénéficiaire génère une page blanche ([10fdce5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/10fdce51b2c43d042f4e67bedd3a0930d79bb0d6)), closes [#181](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/181)
* **beneficiaire:** la navigation entre les sous-sections de la section informations générales recharge la page si le formulaire d'édition a été accédé depuis un onglet de la fiche bénéficiaire ([d7d028b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d7d028b9fda9dd7fd386324802f785af1dfdcb44))
* **beneficiaire:** la navigation vers une sous-section du formulaire ne fonctionne pas correctement sur chrome ([3db8d08](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3db8d0812bc34243e448759a3e2f5a4038cad936)), closes [#180](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/180)
* **beneficiaire:** Masque une section de la fiche lorsqu'elle est vide ([1896b6e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1896b6e3c7123bcb10cfc3d6d7fc4b5b4a2ab649)), closes [#209](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/209)
* **beneficiaire:** N'affiche pas l'alerte de fermeture si aucune modification n'est faite sur le ([06b978a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/06b978a77cd2ae40019fdbceb080a87b74a1e3db))
* Change la règle d'apparition du block santé ([35b9eca](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/35b9eca8cc0707d05b3457c7e1033f843c88e409))
* closes [#177](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/177) ([98c226d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/98c226dbae4f7a1d2a449345fedb6eb51ba41bc3))
* **connexion:** affiche un message d'erreur si la validation du jeton échoue ([18c296d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/18c296da062a10183256ce6fcdab86eb8f37ce25))
* Correction de la création d'un utilisateur ([9c73710](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9c737100221181116fcb299947d80a0a5391359b))
* Diverses corrections mineures de bugs qui permettent de sélectionner une option désactivée dans un sélecteur multiple ([5a4b882](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5a4b882fd85ac3158b0a2f2ad4df23f745d9964f))
* **email:** Les retours à la ligne ne fonctionnent pas sur Gmail ([f85a7c8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f85a7c8ebd580c2e8be247282f4f25380d6ff5c1)), closes [#203](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/203)
* **filters:** pagination bug ([2139ad0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2139ad009166a6553367e0e2df9c5e497cab4f7a)), closes [#190](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/190)
* Filtre les données avant le traitement des requêtes ([a63ab49](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a63ab49c2213333795f8e9afd0c8d2230ae0a030))
* **formulaire:** affiche un message compréhensible lorsque la date saisie est incorrecte ([f51f0e2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f51f0e24f7c3f2b8cb866b4f6a927fcced3e3dd1)), closes [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/67)
* **help-request,followup:** Corrige l'ajout de commentaires ([56978f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/56978f7495b477898070199bf6d5e21f7c5d7014)), closes [#210](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/210)
* le bouton de fermeture d'une alerte temporaire ne s'affiche pas correctement ([b002a84](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b002a84fb32ebb2534ba77bc4645c4c002d2f943))
* lock file ([5f2885a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5f2885a01e748ab62fc1f0eb1e79f54d6b196960))
* Masque l'icône calendrier du DSFR sur les champs de date sur Firefox au profit de l'icône rajoutée par le navigateur ([250d75f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/250d75f7a58fc6b425034c18a8829b408db8c59c)), closes [#192](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/192)
* Règle d'édition des informations de santé ([fe51b01](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fe51b019dcdefcd602cee755f5d9ed29ae37e63f))
* un champ de formulaire faisant partie d'un groupe de champs n'est pas focus s'il est le seul a avoir une erreur ([87fb347](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/87fb3478b5987c37c9e0ac831f6ee024d4dde794)), closes [#206](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/206)
* VeeInput automatic formatting ([01873d1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/01873d18004d27d8de7a0524fb0e80ae5e817d18))

### [1.8.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.8.0...v1.8.1) (2023-10-31)

## [1.8.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.7.2...v1.8.0) (2023-10-30)


### Features

* Ajout du type de structure EFS ([c1eca2e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c1eca2e801a93fb2d740d022ab4cdc1e606a9cf3))
* Une commune n'a pas accès au NIR ([2d01169](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2d011693339819f970f06bede0f8c8cbe6b8c9d9))

### [1.7.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.7.1...v1.7.2) (2023-10-23)


### Bug Fixes

* Affichage d'un bandeau informant du problème d'envoi d'email ([22a088c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/22a088ca0c9f892fca0f914b687e16d134e58ce6))

### [1.7.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.7.0...v1.7.1) (2023-10-23)


### Bug Fixes

* Correction de la création d'un utilisateur ([fa11bfd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fa11bfdcab64d417ffe194f52da3faaed91bf52a))

## [1.7.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.6.1...v1.7.0) (2023-10-16)


### Features

* **accueil:** ajoute un bouton et un bandeau de contact sur la page d'accueil ([8c4cb05](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8c4cb05ac9b33f6723ffc26fb058ad32999c62b9)), closes [#143](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/143)
* Ajout de contraintes sur les entrées des formulaires ([4957334](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4957334f77b3b8a32ab6aef7c17f2f0fa8220e91))
* Ajout des vérifications des règles de sécurité pour les routes ([a5e6e00](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a5e6e00de0aa8dee3a3c1eacda45e68a9a10ce91))
* Ajoute la section Activité/Revenu du formulaire de la fiche bénéficiaire ([26ea759](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/26ea7597b2a2e76fa264cef8528a0fbc3e4146f0)), closes [#149](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/149)
* Ajoute la section Entourage de la fiche bénéficiaire ([00bdf22](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/00bdf22c97f88cb1e5097f297fdb77886ca095ae)), closes [#124](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/124)
* Ajoute la section Santé du formulaire de la fiche bénéficiaire ([330c4f5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/330c4f58c1c09308534b88b2e1ba151213dcb6be)), closes [#125](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/125)
* Ajoute la section Structures extérieures du formulaire de la fiche bénéficiaire ([36a8481](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/36a84814fcecd19d9007a468980e00eb5b9fe7fb)), closes [#126](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/126)
* Ajoute les membres du foyer fiscal au nouveau formulaire de la section Foyer fiscal de la fiche bénéficiaire ([6ada3df](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6ada3df0338f65a14367ab9cb8e887d004b723d4)), closes [#123](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/123)
* Ajoute une étape Foyer fiscal au formulaire ([3f82008](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3f820080a8141eecd1191799d91389a18760f2ec)), closes [#123](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/123)
* Améliore la mise en page de la section Foyer fiscal du formulaire de la fiche bénéficiaire et rajoute le champ "Date de naissance" à cette section ([2e5848b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2e5848bf88dc42b915d40eebb3c3f6af66c85132)), closes [#123](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/123)
* **beneficiaire:** ajoute le menu latéral du nouveau formulaire de la fiche bénéficiaire pour naviguer entre les sections ([dac0251](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dac0251f94fd47069868961838b710d1313b52e3)), closes [#127](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/127)
* **beneficiaire:** met à jour la sous-section active dans la section Informations générales au fur et à mesure que l’utilisateur défile ([e124307](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e1243077cd90e580911be20daa7aafabfd4d8bb4)), closes [#127](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/127)
* **beneficiaire:** retire le champ mandat aidant connect du formulaire simplifié et reprend la mise en page de ce formulaire ([545e56a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/545e56aab18d3183e16f8c35a8e290e2645356ba)), closes [#147](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/147)
* **beneficiary:** add quick action "add button" in sidenav and add required fields indicator ([bdf55f1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bdf55f124e5a1ed100c0bb4d17537fa4031bc379)), closes [#135](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/135)
* Change l'apparence du système d'onglet de la fiche bénéficiaire ([c1fdabc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c1fdabc8693d976e9d283beee84dc06a6851c456)), closes [#132](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/132)
* Change la présentation de l'en-tête de la fiche bénéficiaire et des boutons d'action associés ([9080776](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/908077664ae4c2d47ed4038551ca455157cf8aa0)), closes [#130](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/130)
* Configuration des En-têtes de sécurité ([d7383d0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d7383d00d4bc064469a579432249f71bfcc25142))
* **document:** affiche les étiquettes supplémentaires du document au survol ([43aac23](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/43aac239d3d64a2cc123b65fce8cdf748df12417))
* **document:** améliore l'accessibilité des boutons d'action ([e652f65](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e652f6513f41cda76faaaed0806824d2ccb45630))
* Enregistre l'onglet de la fiche bénéficiaire ouvert au moment d'accéder au formulation d'édition ([3c4fb4f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3c4fb4f723b8de76facf270c8004bab921705594)), closes [#128](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/128)
* factorisation de la vérification des règles de sécurité ([4acdedc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4acdedc0829cb7723f9ea80d8423b43900432788))
* **foyer fiscal,entourage:** ajoute des statuts aux membres du foyer fiscal et ajoute les nouveaux status Entourage professionnel et Ami·e aux membres de l'entourage ([046b785](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/046b785cbc6995a8490b09eedc38833a57f24be0)), closes [#139](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/139)
* **Liste blanche:** ajout des permissions pour les statistiques et l'administrateur ([a9f9ec9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a9f9ec9044eca788c69250d43562dc508b40b690))
* Mise en place d'une couche d'accès à la base de données pour chaque table ([b908ed5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b908ed503931fbad29c5e4fdfa6847f5dcb81135))
* Mise en place d'une liste blanche pour les accès API ([7ec0f6c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7ec0f6cdff69319cb1785cc3972de5cabca19c50))
* Nouvelle présentation de l'onglet information d'un bénéficiaire ([82dddba](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/82dddba6c775138d9e845f7e0d9524b94305cc52))
* Préparation à la mise en place de la liste blanche ([f548e72](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f548e72f3644c399c735d4700754cc764f4cd678))
* Reprend l'expérience utilisateur de la section Informations Générales du formulaire de la fiche bénéficiaire ([b43866c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b43866c16d6958619981d9fbbceaecfe83740147)), closes [#122](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/122)
* Reprend l'interface des documents dans la fiche bénéficaire ([fd4e961](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fd4e96143c93116abb41b03a19014745817e51b3)), closes [#60](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/60)
* Reprend le design des systèmes d'onglets ([18d6d9c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/18d6d9c48c8c662834b73b4d4196bb48a957f427)), closes [#132](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/132)
* Retire le nouveau champ Lieu-dit, commune déléguée ou boîte postale ([d844eff](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d844efffaccf44a7c7fd3bf18d20995e09dba7a5)), closes [#122](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/122)
* **seed:** ajoute un compte administrateur par défaut pour travailler en local ([34c55b4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/34c55b4d761877b8327e4f2c0621e3da1fc20a23))
* valide la saisie d'un champ uniquement après l'avoir quitté ([1b2740d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1b2740d3ff53d171c4d81aa0315150aae5d6853a)), closes [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/67)


### Bug Fixes

* **a11y:** améliore l'accessibilité des liens externes ([6bda729](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6bda72926cf8f73f54acb19b5614ff55f93fbe0b))
* Ajustements UI sur la nouvelle fiche bénéficiaire ([fbecc43](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fbecc43f9312ae147dcb366e2d018fbd347cb5af)), closes [#131](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/131)
* **beneficiaire:** améliore la mise en page de la section entourage du formulaire de la fiche bénéficiaire ([4b6ef34](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b6ef34d1f93b2b9cbbef04bc722f0d1a5f25653)), closes [#124](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/124)
* **beneficiaire:** corrige des libellés d'informations de la fiche bénéficiaire ([62e7b34](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/62e7b344a99f57e11aee7f2e5e19b5ad2c418ab4)), closes [#125](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/125)
* **beneficiaire:** impossible pour un agent d'accueil de charger les filtres relatifs aux demandes d'aide ([baf42e1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/baf42e16dab9eefc83a0655ccdc43955e6d1d74b))
* **beneficiaire:** le second numéro de téléphone n'est pas affiché ([a96a4fb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a96a4fb11636c12d2e5f1f81791e7e6a70bba950))
* **beneficiaire:** lors de l'accès à la sous-section Mobilité depuis la section Informations bénéficiaire, celle-ci ne reste pas active dans le menu de navigation ([f3fa858](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f3fa85827610ccff29eba26e05dec6da2dfe0095))
* **beneficiaire:** retire l'ombre portée des blocs de l'onglet historique ([a19c5e6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a19c5e69f686f0bfad6c53a91591e0ddf66c95b9))
* **beneficiary:** améliore la mise en page de la section activité/revenu du formulaire de la fiche bénéficiaire ([ef24baa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ef24baab8653a7ca79d5378c7f29d4348ee64d54)), closes [#149](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/149)
* **beneficiary:** file number in details metas ([5f5d36b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5f5d36bf01f0c697c2893d8a875db12f7f52ae29)), closes [#130](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/130)
* **beneficiary:** referent role can save beneficiary with complete info ([bac2104](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bac2104fb7b54fee5884e5cec6b786d3a1c28e96)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/135#note_1545546050](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/135/issues/note_1545546050)
* Corrige l'affichage des onglets de la page Accompagnements ([196eba1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/196eba1c1df74dfa8b0fa78cf82c2ec90a86f19c)), closes [#132](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/132)
* Corrige le message d'erreur lorsque la modification d'une fiche bénéficiaire échoue ([dbe7760](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dbe7760c56ea32e6584a7f9da20d13bfca579e43))
* **demande d'aide:** impossible de modifier une demande d'aide ([df127b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/df127b2083cfc8c51a54230dd89d6d71fc5f338e))
* dépencance cyclique ([c06f90e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c06f90e61d01a9ee9a37f8abc63fa6a2d7de95f8))
* does not return handlers from route creation ([1d0670f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1d0670fa576a8589a116915e66c939d090d81d1a))
* **followup:** the success message after uploading a document to the followup is wrong ([63be16a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/63be16a815f8751c21331668e2aa0f5bb404d1ea)), closes [#136](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/136)
* **formulaire:** problème d'affichage sur les champs de type date lorsqu'ils sont désactivés ([d87bde6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d87bde65cda6441b1909ac39521613a31dcacec8)), closes [#164](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/164)
* Impossible d'accéder aux statistiques ([e4be399](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e4be3993e47422c570eb21be8325f0aebe37cfe7))
* Impossible de vider le champ Numéro Pegase ([132cd0d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/132cd0de45e4e4debdc035882dab4139d18d05db))
* impossible de vider un champ optionnel avec un schéma nécessitant une longeur minimale ([fc6a76f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fc6a76f158141dd067a864b160ea23b7649ffccf)), closes [#107](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/107)
* **Inclusion Connect:** au retour d'IC, l'email est mis en minuscule pour faire la recherche coté MSS ([71eca89](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/71eca89288ffd5d12cbebc2a24d5408bbb7b8a9d))
* Lors de l'annulation du formulaire d'édition d'une fiche bénéficiaire, la redirection se fait sur la fiche bénéficiaire ([acc5c6a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/acc5c6a730ec1ea09cbbc2fba6e3eb05b736e1f1)), closes [#128](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/128)
* **seed:** beneficiaries does not have any referent ([1b05e18](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1b05e189f399b8d8123dc1028d5c57ebb19932ed))

### [1.6.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.6.0...v1.6.1) (2023-09-26)


### Bug Fixes

* Impossible de saisir un montant de ressource avec des centimes et de saisir 0 enfant mineur/majeur ([9b68ce5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9b68ce5b91c5288600f79e30a5ff4bd3cb05c95a)), closes [#145](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/145)

## [1.6.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.5.2...v1.6.0) (2023-09-04)


### Features

*  beneficiaries export ([28cf1d7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/28cf1d7d17e8f80f1ac9219a849930c73519121d)), closes [#55](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/55)
* **a11y:** adapt title for each page ([1f0ba7e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1f0ba7e09a857da3253b9c8cfa011c2f872c1f38)), closes [#87](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/87)
* **admin:** améliorations espace admin ([9d200fa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9d200fafafeb939b8b3e340911cad8af37516d79)), closes [#103](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/103)
* **admin:** Problèmes de création d'un utilisateur depuis la partie admin ([b497867](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b4978671be9c1abd333919850f1b63f028e55604)), closes [#102](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/102)
* Ajout d'un ruban indiquant l'environnement ([f572be8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f572be89c90284de3ffe8833c3bc19e95f7e6ef2))
* **beneficiaire:** Gestion du formulaire complet de création d'un bénéficiaire ([64e3f69](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/64e3f694bac38b725b86364790a4e07e52f69ce4))
* **export:** add followup count, followup type total for followups and help request count ([d40f115](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d40f115841a428be68dc5ef8d357fa7fba1885b3)), closes [#56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/56)
* **export:** Ajout de l'export des statistiques ([a7ea908](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a7ea908bcb8b407ce5593ccdc9fb19ea0a6763c3)), closes [#56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/56)
* **export:** external organisation column name ([d81e1af](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d81e1af8a0272555cf3e82035637e3c9cc3e0214))
* **export:** Résolution des retours sur les exports d'historique ([76ff8ca](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/76ff8cab7b5599b22d93992aea656322ac73aee0)), closes [#54](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/54)
* **followup,help-request:** update ministers after the last cabinet reshuffle ([b6ba784](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b6ba784da23941dcfed990700ee50ab72e9ca90c)), closes [#99](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/99)
* **history:** add export to Excel sheet ([dd58721](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dd58721557c7f01356ca2b56313468f8eb05d6a4)), closes [#54](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/54)
* **matomo:** add matomo events ([776c393](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/776c3937a25b0d6c0b64287445f6bd3ad7eb8bbb)), closes [#65](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/65)
* **matomo:** add tracking on the document edit button ([580c234](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/580c2342d9c4620f6cb8e920bcb17618bb33d653)), closes [#113](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/113)
* update roles table for history export ([0548ed7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0548ed78efa8c3e5193c10d37c3480c34107ffe1)), closes [#54](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/54)


### Bug Fixes

* **beneficiaries:** age group filter is not working ([8943335](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8943335ed3bd9549befdc06716442d9d22f23bd8)), closes [#117](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/117)
* **beneficiary:** the accommodation mode additional information input is shown even if the beneficiary is not hosted ([1e2ec05](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1e2ec05287c5180e745f7eebe9c6675eac21f2a2))
* can not edit additional information of beneficiaries ([64ab031](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/64ab0317d443f74eda45ed665847eef512f8daea)), closes [#105](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/105)
* death date error ([968718e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/968718e800d4e7b2df571f6896fb96a1d53d11bf))
* **export:** correct stats export permissions ([0a6a340](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0a6a3401663a97c6cf149cf8806ebb13e2c919ba)), closes [#56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/56)
* **export:** minor fixes in the beneficiary export ([cb45cf8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cb45cf8ec1f68dc00d71f50e9c4fdb21fc518009)), closes [#55](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/55)
* **export:** minor fixes on history exports ([9e67dcc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9e67dcc45b5471019ec6e6b6a7701656b8b9b027)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/54#note_1533726687](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/54/issues/note_1533726687)
* **export:** resolve beneficiary export issues ([d48c2e4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d48c2e4fb663268594238886714dffe553edc0f0)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/55#note_1533255992](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/55/issues/note_1533255992)
* **export:** use the dsfr button instead of a link ([c771f98](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c771f98b6e8bea64aac330d6421c427eb346c461))
* **export:** wrong column label for the followup referent ([12539b4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/12539b47f48d593e6c05d3624bc1a4d05af09814)), closes [#54](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/54)
* **filters:** examination dates and cities filters only includes accessible data ([880f9e0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/880f9e02c3ff20faaaedad69ab4961a9e0cbc602))
* **filters:** hide referents filter to referents ([289f704](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/289f704fcc3eefcfed97f60419ee179647927662))
* **filters:** referents can not filter based on referents ([7448195](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7448195ee3d465afbefb8f9bc107a03fcd54b3c4))
* **permissions:** stats export is wrong for referents ([f5c0f1b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f5c0f1baddd7487d93139f62e58bbf01a28d0d23)), closes [#110](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/110)
* remove dead link ([d2d06d4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d2d06d40cde40aa63d238489c342ebb65334459e))
* Sauvegarde du champs Mode d'orientation dans l'édition d'un bénéficiaire ([11708d6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/11708d6e48fadd10d7e5bf803f252a2000aa6b71))
* schema imports errors ([ce65ad3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ce65ad3ad5f9b5944c239d1ca5a8d7dbbb5204b9))
* **stat:** first follow up stat does not take into account the referent status ([6ce621a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6ce621acf3a3a211e99b277aa93cfb23c9f72a8c))
* stats permissions ([b91b9a7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b91b9a764ddb96bb151e2b14248b95fc61111f94)), closes [#110](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/110)

### [1.5.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.5.1...v1.5.2) (2023-07-27)


### Bug Fixes

* **matomo:** add missing events to track beneficiary creation ([c4e3c94](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c4e3c949839398d2a57e7243581013c332a53bec))

### [1.5.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.5.0...v1.5.1) (2023-07-25)


### Bug Fixes

* **followup,help-request:** missing current ministers ([dbd7263](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dbd7263c0a7430b021c95997e90563958646840c))

## [1.5.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.4.1...v1.5.0) (2023-07-25)


### Features

* **followup,help-request:** update ministers after the last cabinet reshuffle ([5c0f6fe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5c0f6feff72b3755dad6a8767faa1ad1f99a7bfd)), closes [#99](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/99)
* **matomo:** add matomo events ([6de414a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6de414a328f8b5cc1302333cbd9ab01024536f72)), closes [#65](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/65)

### [1.4.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.4.0...v1.4.1) (2023-07-24)


### Bug Fixes

* death date error ([c43c363](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c43c363d0adaeebc8a45a48a6102b700ace75bbf))

## [1.4.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.6...v1.4.0) (2023-07-24)


### Features

* **beneficiaries:** sort beneficiaries by the usual name by default ([6c45830](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6c458303a1cb9a9c2831369c870c664344269f3c)), closes [#80](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/80)
* **followups:** add date column and remove medium column ([850d0e4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/850d0e4e6d11dd627c2ee02c0437fa835e28b2cf)), closes [#78](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/78)
* **help-request:** update the label of the accepted status in help requests for minister structure ([56b869c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/56b869c9b3ee3964ccbed17705e9b060f9ff454d)), closes [#83](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/83)
* **history:** add minister filters for minitry structures ([0cfcf80](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0cfcf8052f37e74f4ff01cf7155c7e924c3c491e)), closes [#83](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/83)
* **seed:** add minimal seed ([bef53e3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bef53e3b222e39f0646e68268aa49a3dc573025e)), closes [#89](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/89)
* **stats:** sort stats related to followup types ([d57cdf2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d57cdf298d698d81e8f609dfe5a9a171c8708849)), closes [#79](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/79)


### Bug Fixes

* **admin:** impossible to create an admin user not linked to a structure ([1e141bd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1e141bd55a93fcbb43f6107f5720e9f229c9c787)), closes [#88](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/88)
* **beneficiaries,help-requests:** filtering by age group fails and cities and examination dates contains other structure data ([7ed1934](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7ed19342b87173271b20a1301bc3cfaa94793650)), closes [#98](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/98)
* **followups:** the follow-up types row is too wide ([184ee1c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/184ee1c6c64a10b95df4f0c66b9cbca8b24430d1))
* **help-request:** status for ministry are mismatched ([831e059](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/831e0598464633ca6f62ba5afdfbe75fc248ee56)), closes [#83](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/83)
* prisma updated date not updated ([934b6b1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/934b6b16d8d0305b2217c5d47bd598ffde7d5ae0))
* **table:** change the label of the sort button for numeric sort ([f2a1752](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f2a17527c5f26ee80b993c4364c7ffad02ce0e44))

### [1.3.6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.5...v1.3.6) (2023-07-19)


### Bug Fixes

* **email:** remove the incident banner on top of the user form ([7d8fafa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7d8fafa8806b44abeebfc0899bae5e27521fa0d2))

### [1.3.5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.4...v1.3.5) (2023-07-18)


### Bug Fixes

* **email:** update the incident banner message ([381f2f2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/381f2f2b92180bc46f824ac63c5387bac1cffb97))

### [1.3.4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.3...v1.3.4) (2023-07-18)


### Bug Fixes

* **email:** add a banner on top of the user form to inform about the brevo incident ([735debe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/735debe8ff4453f577710744a7ff2de7b7df30db)), closes [#91](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/91)

### [1.3.3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.2...v1.3.3) (2023-07-12)


### Bug Fixes

* **admin:** tous les utilisateurs peuvent accéder à l'interface d'administration ([05e3958](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/05e3958c1c4b018470105400e9ee030d55aaa03e))

### [1.3.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.1...v1.3.2) (2023-07-10)


### Bug Fixes

* **logout:** remove user from auth store ([9baa27d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9baa27d04336deeaaf87973fe64019f99637f2e1))

### [1.3.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.0...v1.3.1) (2023-07-10)

## [1.3.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.2.0...v1.3.0) (2023-07-10)


### Features

* **document:** set added document into followup and help request ([69ee23e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/69ee23ebd64c8485093d48383daea688a88c2511))
* **tax-household:** remove deletion action ([dc45960](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dc4596013866aeac17e3d23293f04f30ea0fa968))


### Bug Fixes

* environment variable is not set ([bb9bff0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bb9bff0c2503ee7d91ef54957090d6decd309a5b))

## [1.2.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.1.1...v1.2.0) (2023-07-10)


### Features

* ajoute matomo ([c92afa8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c92afa83c7a271ca30fc50c048eb76392e10bff3))


### Bug Fixes

* **beneficiaire:** ajoute le numéro pégase au formulaire de création de bénéficiaire pour les structures de type Ministère ([0d3d7fc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0d3d7fc4687fb2b7f0f7beac259efb8cac47549c))
* **beneficiaire:** corrige un bug qui empêche de vider le champ code postal ([132c281](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/132c2818d29f1f0c21de833977d7c1d142e6faa7))
* **formulaire:** Affiche des erreurs intelligibles sur les sélecteurs ([4509659](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4509659656b13ef8f308dd4201ca4b33fef7b8de))
* les tags de document ne sont pas correctement affichés ([bce44a8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bce44a8c5f3457b0cf255ba081c03e9ec0cb74a6))
* **logout:** call inclusion connect from client ([4751305](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4751305a492e0c7115c966b531c95ebeda703f01))
* use custom validation for zip code ([d0fe83f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d0fe83f2e0c7243f28352cb41c629d30073d195d))

### [1.1.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.1.0...v1.1.1) (2023-07-06)

## 1.1.0 (2023-07-06)


### Features

* add accommodation zone stat ([86a3389](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/86a3389a60bfc6231dd2d68954245a8974fe8f41))
* add account page ([5875a05](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5875a05bd0ef4bd60e7643a3673ecd666cb62153))
* add beneficairy button action with access to add a followup ([2a63c1a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2a63c1a2d5673f638cc606794c3ead3bc00b46be))
* add beneficiary and history item brute delete ([74394f1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/74394f1750bbb8b76b658cb53f3aa36f479e2438))
* add beneficiary history ([a186517](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a186517eed911feddaf986cefc0b8d0ce6096a6a))
* add collection mutation logs on beneficiary page ([3844a28](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3844a28a14c567f000c0f02980812952db2c2b37))
* add createby and updatedby on mutable models ([8b2a4fa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8b2a4faf0538f811102f3a026ab6f60e128b9b17))
* add document ([3931e6e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3931e6e60741bcdb1ee99834a8bcb27c61457cc5))
* add due date and end of support notification generation ([c6fc73d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c6fc73d2a9cc3a65991f0bd69efdbfe4f87b4c78))
* add edit permissions to the help request table ([5e03655](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5e036557c79c1b8c8b2b48a0ab6ab2eda3baf70c))
* add followup tab in history page ([c9fd6a8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c9fd6a80e938bbd93ca63008e3c7ef3e2c55d46b))
* add help request tab in history page ([be2d293](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/be2d2935641dcf9e461791ec9093995681b64bf0))
* add instructor organizations to the help request table with sort ([d5aa4b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d5aa4b2e33f6d85cfc478f3acfb5f68bb2bd0b2f))
* add license ([126a42f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/126a42f4bf658bcb49fad9150a3976e2f7287d23))
* add middleware to skip auth for authenticated users ([96074ed](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/96074ed8ff900576dfd3917638b528ec939b8da2))
* add ministere departement structure ac stat ([502fea8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/502fea817bc82a940312668be68c02e76ac508b6))
* add ministere structure stat ([24226c2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/24226c2a37f94a03fd10e915a59c73bdae00e05e))
* add missing instructor organization stats ([c12dc2c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c12dc2ca9b101d859eec5f026826724540b0a3ea))
* add missing rule ([63205f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/63205f7ed5e6e3d7769fec13e077a960483d9d4a))
* add notification generation one month before a due date ([20300c0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/20300c0527427bf66975ce591ab7bd20e2e68009))
* add page for disabled accounts ([0c3ac62](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0c3ac62f662c1742d008379e3ef0631c12d32c1d)), closes [#13](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/13)
* add pages to create or edit a help request ([e4c003c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e4c003cc92d0b1a1e3dd89eed2c50f6158cc63cf))
* add print page and other information section in beneficairy details ([4171e96](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4171e96650c26c9568584e6264571e06dc83f059))
* add structure page ([046d0f4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/046d0f4e63e447f730a3698b28844b9978c5e5b8))
* add users page ([12a76d7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/12a76d74ffe8c0b30f2c2c3611abde56c48ffff3))
* admin pages for structures ([d015e57](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d015e5716517a9ba17e07636e871ffcd865ec58d))
* **admin:** add overview ([93455ee](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/93455ee2ee5fb0e5af73ecab6ab27a584a0e2663))
* **admin:** create admin space ([5c85ce2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5c85ce2f16d3336574c2a8222441a9be1d9c9b39))
* **auth:** implement properly auth store ([a85a3af](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a85a3af58430af980043ca82fef68c94aca0bd1a))
* beneficiary detail page ([9df5d2a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9df5d2acb4312b3b3e8022e176328e4ac0c2b41c))
* beneficiary edition ([66f2b52](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/66f2b5229fe4d26b5cac21892254be00129b6329))
* beneficiary form edition ([dd4e985](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dd4e9856b1c50b064a7bc1262e9ec0ec275e60ff))
* **beneficiary-details:** add followup types ([31b82b8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/31b82b81b2285de4873f32359617ffa176658816))
* **beneficiary-history:** add beneficiary history tab ([b1d89dc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b1d89dc3dda540bf93b22b4ad5dfe7cebea5a8a4))
* **beneficiary:** handle creation ([1abcfbd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1abcfbdde5c2de8fcc364030d5da270c33e3e8a5))
* **beneficiary:** start implementing beneficiary page ([8b8a2b4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8b8a2b49123ce02c77599f8587baa8f95e14241b))
* **cgu:** reccueil de consentement des CGU ([71fdf43](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/71fdf435233956a135a19f3110b16faf64e1a419))
* clean roles and permissions mess for beneficiary ([c0cc992](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c0cc992f5729d817178f2edfea9bbd8e77238f57))
* clean roles and permissions mess for stats ([3e9b4f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3e9b4f7f0c7391023f19a910ee30de0a9d822f44))
* clean the whole roles and permissions mess for users, structure and beneficiary ([80a9989](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/80a99893b10c4c2852c26bacd11fa31882326d77))
* complete btn in beneficiary creation form ([5194048](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5194048583d54c858b3875e5c0b74174df03d4a5))
* **document:** add confirmation before deletion ([4b7a46c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b7a46cc63575954c0149f5fca36d93da86525bf))
* **document:** display document list ([cf7bd3e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cf7bd3eda092d506cae6c69b005f48bb4cea6675))
* **document:** handle filename disk to maintain compatibility ([dba7a20](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dba7a2042b7dc7c5a02f6646a5389e2c81f3b992))
* **document:** upload and delete document ([1c19951](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1c19951be33ccc2e2d25472bc7e13229e09b78b6))
* dsfr js scripts ([1860f55](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1860f55aac686dca75336c969e9be8066466ff89))
* edit document ([17897ab](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/17897ab9c6f4be17121e61b46ab225ba74681998))
* **edit-beneficiary:** use vee-validate ([4d8210a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4d8210a66e455d47a19426263948d906945d9d07))
* **followup:** implement followup creation ([b001ca7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b001ca75bee00cb90ba00396bd0fe7f82c004919))
* format beneficiary names to a human-friendly format ([2c5b800](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2c5b80065ebb83646e98aa865325b06dca17abe4))
* handle relatives in beneficiary edition and update schema ([3420129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/34201295a285820a32748c3855b4a6d70d440af9))
* **help-requests:** update help request status wording ([07895e6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/07895e6670e0286a73621f02d8be273715de5805))
* implement beneficiaries page ([08047eb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/08047ebbe8079ff30b530b80b1a8fe0b41f9d299))
* implement page showing variables ([0f1e1db](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f1e1db3ff16e4c929d0581f7ff605380eb30181))
* introduce page container component ([fcdb771](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fcdb77104507801d680b0354ec64ddf4d181dff2))
* **jwt:** handle refresh token ([ade6e01](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ade6e01350a5bd24c746b3d2cbcdf884fe4399b6))
* lock access to different section in the beneficiary based on the permission rules ([4633d15](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4633d15bc8d539ea58adc57c2628914bc6987a0e))
* **logout:** logout after inactivity too long ([6be4d7f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6be4d7f001798a2c237ac36a4a45af1cae3864ad))
* make trpc protected procedure context safer ([8de8f19](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8de8f19825b2666b3648c4cd4b3ff16f2a84c130))
* merge two enums ([c708307](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c708307ab79d3c35a09b80a47c26be7b5e59479e))
* **notification:** display notification in dashboard ([2b73830](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b73830d737fcb7bc93db04713da51abee54f5f4))
* **notification:** display notification in dashboard ([3c58976](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3c589767b568c53ef825e1448659af78ba5a71f1))
* **overview:** beneficiary search bar ([07eddcd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/07eddcd269b418040049fe323516cdd3c7fce44b))
* redirect to the url the unauthenticated user is trying to access ([edc155b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/edc155b16a826bba847a483f5c6e91739865915d))
* redirect unknown user to an error page ([b107ce0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b107ce061531352b7e7dbed7e39e4641d992d376))
* **s3:** s3 lib and document handlers ([6729bce](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6729bcec73aa86e675e5fc9e840900a3d2968529))
* scroll to target element when navigating accross pages or tabs ([a47f3f6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a47f3f6af713ea61739198b73c9b1cd6b3334e34))
* **search-beneficiary:** search beneficiary input ([510dcb1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/510dcb1e6784b590b7844ef86b2a937980b0b97b))
* show linked documents in history ([07ffd87](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/07ffd873c9bf39f6b0eb56ae3a1a2fd266d0a52b))
* stats filters ([6e85147](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6e85147959d9b3f0a285a3c585bb58c343452bfe))
* stats history ([37e0b2d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/37e0b2d4c6400fd13f5a9d6fcba732a290d5dc05))
* **tab:** use vue3-tabs-component ([597aee0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/597aee05ac080dbbed7cc4f199dea97c9be6b946))
* **trpc:** add transformer ([4f99885](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4f9988532561aed4c4810c16b5a34db27aff4932))
* update breadcrumb ux ([19e4510](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/19e45101cef1809d84c6774ab6cc59df01fa45a8))
* update token parsing and migrate to the demo realm ([4f48c8f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4f48c8f869a81a10a744d9942290fb77fde91517))
* update vue-dsfr ([5685102](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/56851021dd80e6c9608d0b0ffdb0a56dc26670ff))
* use security rules ([4a9ae48](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4a9ae488bf7bfe3e064911a7292d543650d09f09))
* use vue-tabs-component in history page and rename tabpanels for better genericity ([8ac129a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8ac129a0805282d76c2f59f45df20ad402370711))
* useMinistryAgent ([7fe3d8e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7fe3d8e7037e33d3e7ab093380c8da411069765b))
* **user-activity:** introduce user activity ([2ebeaa0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2ebeaa02e6f2e0851100ad13d99eabcc1da31e1a))
* **users-activity:** replace mutation logs by user activity and add pagination ([5324b4a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5324b4af8b5da4d480acd0966d68c628b02f4d7f))
* **user:** send email at creation ([e4180b6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e4180b61f1b4835912f0f87d27cc0b325a616cba))
* **user:** update email content ([ecc21bc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ecc21bc9fd6375165f5a4a780e7b1bddcfd5eeb6))


### Bug Fixes

* add social security number input ([8427cbb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8427cbb0f845d24c3c5c7de19fb8053a9eb0010c))
* adding structure follow up types ([7ad63c0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7ad63c002a68b467d0ce123cd6d94caa8f155c71))
* **auth:** fix conflicts resolution ([cc82c16](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cc82c164cf64793c7d483c8047ec4398cec2d89c))
* beneficiary deletion - closes [#63](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/63) ([781e1b7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/781e1b75f9f8255e0f580c25048fd570b04f1216))
* **beneficiary:** extra spacing around the info alert in the creation form ([13e74fd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/13e74fd470750a008ed8dee3d2fd1c1190222dda))
* cannot add a comment to help request ([a624214](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a624214bc78eeefcbe78d62edb06e9bb61933754))
* closes [#64](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/64) ([cbb1ddb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cbb1ddbb59b663599e6178b42ed55e2c68525346))
* confidential document rules ([8ad62c9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8ad62c9efd5a9d1ed0dc2c2a7847b649ae499034))
* disabled agents in beneficiary add form ([342c8c0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/342c8c00ae4b016117f2e2a6e40b3b6d781fdeb9))
* **document:** use correct event ([f16e00b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f16e00b5a8bbc488b5363a1795153cccbaf52b67))
* examination date bug in filters ([9094fb5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9094fb50084554deb7a349f90fbde6609dfb188b))
* filters for supports stats ([52c7f2e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/52c7f2e29d5852d4aff4c2210b5143e8db2a40c0))
* force using stable pnpm version ([13e351f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/13e351f8b3ff9a1be4d5443e0addc2973f8ad236))
* import ([f545d60](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f545d60049c23558be3c4d06eec9cbae1274a01c))
* impossible to access a beneficiary from the searchbar ([f886d70](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f886d705dec5e4ba88b77201b6516e3c1d6b51d1))
* impossible to edit a help request ([45fb6b7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/45fb6b7a6fb442502a6796f6d00554daa13fb09f))
* **inactivity:** logout only if process is on client side ([4ef2499](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4ef24999570ec8a13e38701067536914521f6b0d))
* instability due to the client directly accessing to prisma ([ea38ab1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ea38ab1f263411264182fba146179b594219919c))
* invalid page file ([1d60bf9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1d60bf933bceaef35eda9088ccafa007549befe7))
* javascript float number magic is applying to allocated amount sum ([cfa43ab](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cfa43abf977de70449fb7f4de7a95d969dd4dbf9))
* logout success text ([192901a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/192901acfb4c7c6bf7e8d4ad20a3afa64b5e45e3))
* make linter happy ([1d028f2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1d028f24195af2286bbe62dcb3c0644f9a3ddb7a))
* make skip auth working on a direct navigation ([c99b448](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c99b448b2d94f1b9fccd00458829a96a05699e0b))
* middleware for editting a followup is based on wrong param ([8c5ddfc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8c5ddfc84d13b4672b8b4202f3f938a5e185f63f))
* nuxt config ([e4d05cf](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e4d05cf3dbd91bd38df0c775b1f39ff498d0e293))
* prevent crash when user is unknown ([d470b78](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d470b7845db6a26ec6a932a4c637d36c34cf1752))
* **prisma:** prisma client workaround ([b007d96](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b007d96fa120d00fa9020d937c810e6bdf2f2204))
* reactivity issues and pagetopbar accessing user before authenticated ([4965040](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4965040e6ad4fb35d98a957c6e86dbe6b657d629))
* redirect to the correct url after the token validation ([bf499ab](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bf499abc61b29ff1d5ca2394d81c4f46d83b5e46))
* **refresh-token:** avoid infinite loop ([8493bfe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8493bfeaa94727e78f183fc9df2023fcc4e0cc2f))
* relatives labels ([d922bc1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d922bc19cd326257b677721197b6113ec5c30271))
* required email alert on beneficiary creation ([2d06df4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2d06df4e46e68ce7ff979d60431604dd62a39c62))
* return button from beneficiary searchbar is wrong ([39464f8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/39464f8829eb8ea2107bfc5ea6fc93b1fef53dc7))
* **route:** missing route rename ([b3bcd67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b3bcd67ace2dadda1e2a68bc5b03ad022156bf76))
* sidenav ssr error ([6812fe0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6812fe0b25bceeaccb3050df6d9617a15b763574))
* sort custom followup types in the get-structure  route ([e7b9278](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e7b92784e0074dcdcf405fa9ab6503197c80f264))
* sort followup types by name in followup pages ([e677075](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e67707521a7df06f25934df0184d6c892aaa03ec))
* sort legally not required followup types in the structure edition form ([2c19b3a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2c19b3a1d6ed01227b1da8db052d87171dc9e162))
* sort mutation logs by date ([ac54f6a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ac54f6a4f17826253830e35592e31a6b5c2c4166))
* stat access and requests ([0c2182a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0c2182ad8520ac6ff09c2d600d172217565fd540))
* stats issues ([bf749b5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bf749b59741afc472230dc85ffbfe070109aa8ee))
* structure type is missing in the user in the session ([3f07304](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3f073047accaa3caeb83adac94c591834a71249c))
* **tax-household:** select a linked beneficiary is not updating the value ([8918aa5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8918aa533d0182ce84f72a5a1533f982b9b3c7c0))
* temporarly remove cgu middleware ([0b5718b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0b5718bd6adfeb6f7702e7449328fb19d6c6fa9c))
* the user in the store has no structure id ([951b581](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/951b5814c42a511f4bf6f405fc698e21ba95b6e4))
* ts issue ([72741d7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/72741d7796ab718e51c612377f5ad2757fc0855d))
* ts issues ([abb4211](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/abb421194772279970ecc1681a487667bd786aee))
* ts issues ([04e8412](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/04e8412cdf2b16d53917684bc6d5ee0b4f1dee06))
* type ([e393759](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e39375962996c0214d377e1478f6552ce65cf515))
* type import issue ([7475be2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7475be22817af4161d6082f1f7e483e02400108e))
* unwanted horizontal scrollbar on all pages ([7922d5e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7922d5e64be4ab331aa5e2c449ad529c2e55953b))
* unwanted notification on history items and documents ([8f72255](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8f722553e44ddd89853f2a9b59a1003feab064ae))
* useless first level in the breadcrumb ([a74bee5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a74bee589720cc93004b6286b8359c7a74a8e23d))
* user is still considered as login after inactive logout ([b8b6ea8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b8b6ea800160b96d12d29de71d04349a87c39537))
* users activicty not initialising properly when directly accessing the page ([6b29e96](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6b29e969edf274dc2f5f0188c8899a419b1d85fa))
* users that can not access an added document are notified ([0f6447c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f6447cfdf6c3671e6342be2b8ee1f88ed7888db))
* **validate-token:** validate token only client side ([064e0fe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/064e0fe57df2cbc9f3dc0396f75ada7aaf081db5))
* wrong activity type ([8d83376](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8d833764465c433a5d883048667c4628bebe90bd))
